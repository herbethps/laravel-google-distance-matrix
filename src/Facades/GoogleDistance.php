<?php

namespace Hpsweb\GoogleDistance\Facades;

use Illuminate\Support\Facades\Facade;

class GoogleDistance extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel-google-distance';
    }
}
