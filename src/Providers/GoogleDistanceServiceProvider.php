<?php

namespace Hpsweb\GoogleDistance\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

use Hpsweb\GoogleDistance\GoogleDistance;

class GoogleDistanceServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/google-distance.php' => config_path('google-distance.php'),
            ], 'laravel-google-distance');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/google-distance.php', 'google-distance');

        $this->app->bind('laravel-google-distance', function ($app) {
            $client = app(Client::class);

            return (new GoogleDistance($client))
                ->setApiKey(config('google-distance.api_key'))
                ->setLanguage(config('google-distance.language'))
                ->setRegion(config('google-distance.region'))
                ->setUnits(config('google-distance.units'));
        });
    }
}
