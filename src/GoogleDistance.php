<?php

namespace Hpsweb\GoogleDistance;

use GuzzleHttp\Client;
use Hpsweb\GoogleDistance\Contracts\DistanceContract;

class GoogleDistance implements DistanceContract
{
    const RESULT_NOT_FOUND = 'result_not_found';

    /** @var \GuzzleHttp\Client */
    protected $client;

    /** @var string */
    protected $endpoint = 'https://maps.googleapis.com/maps/api/distancematrix/json';

    /** @var string */
    protected $apiKey;

    /** @var string */
    protected $language;

    /** @var string */
    protected $region;

    /** @var string */
    protected $units;

    /** @var string */
    protected $country;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function setApiKey(string $apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function setLanguage(string $language)
    {
        $this->language = $language;

        return $this;
    }

    public function setRegion(string $region)
    {
        $this->region = $region;

        return $this;
    }

    public function setUnits(string $units)
    {
        $this->units = $units;

        return $this;
    }

    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    public function calculate($origins, $destinations): array
    {
        $payload = $this->getRequestPayload([
            'origins' => $origins,
            'destinations' => $destinations,
        ]);

        $response = $this->client->request('GET', $this->endpoint, $payload);

        if ($response->getStatusCode() !== 200) {
            return ['error_status' => $response->getStatusCode()];
        }

        $reverseGeocodingResponse = json_decode($response->getBody());

        if ($reverseGeocodingResponse->status != "OK") {
            return [
                'status' => $reverseGeocodingResponse->status,
                'error_message' => $reverseGeocodingResponse->error_message,
            ];
        }

        if (! count($reverseGeocodingResponse->rows)) {
            return $this->emptyResponse();
        }

        return $this->formatResponse($reverseGeocodingResponse);
    }

    protected function formatResponse($response): array
    {
        list($km_int, $km_text) = explode(' ', $response->rows[0]->elements[0]->distance->text);

        if($km_text != "km")
            $km_int = 0;

        return [
            'distance' => [
                'metros' => (int) $response->rows[0]->elements[0]->distance->value,
                'km' => (float) str_replace(',', '.', str_replace('.', '', $km_int)),
                'km_text' => (string) "{$km_int} km",
            ],
            'duration' => [
                'text' => (string) $response->rows[0]->elements[0]->duration->text,
                'seconds' => (int) $response->rows[0]->elements[0]->duration->value,
            ],
        ];
    }

    protected function getRequestPayload(array $parameters): array
    {
        $parameters = array_merge([
            'key' => $this->apiKey,
            'language' => $this->language,
            'region' => $this->region,
            'units' => $this->units,
            'random' => random_int(1, 100)
        ], $parameters);

        if ($this->country) {
            $parameters = array_merge(
                $parameters,
                ['components' => 'country:'.$this->country]
            );
        }

        return ['query' => $parameters];
    }

    protected function emptyResponse(): array
    {
        return [
            'lat' => 0,
            'lng' => 0,
            'accuracy' => static::RESULT_NOT_FOUND,
            'formatted_address' => static::RESULT_NOT_FOUND,
            'viewport' => static::RESULT_NOT_FOUND,
        ];
    }
}
