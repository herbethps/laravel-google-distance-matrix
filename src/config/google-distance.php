<?php

/**
 * Set your google api key.
 */
return [
    /*
     * The api key used when sending Distance Matrix requests to Google.
     */

    'api_key' => env('GOOGLE_MAPS_DISTANCE_API_KEY'),

    /*
     * The language param used to set response translations for textual data.
     *
     * More info: https://developers.google.com/maps/faq#languagesupport
     */

    'language' => 'pt-BR',

    /*
     * The region param used to finetune the geocoding process.
     *
     * More info: https://developers.google.com/maps/documentation/geocoding/intro#RegionCodes
     */
    'region' => 'BR',

    /*
     * Specifies the unit system to use when expressing distance as text.
     *
     * More info: https://developers.google.com/maps/documentation/distance-matrix/intro#unit_systems
     */
    'units' => 'metric',
];
