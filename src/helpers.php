<?php

use Hpsweb\GoogleDistance\Facades\GoogleDistance;

if (!function_exists('google_distance')) {
    /**
     * @author Hpsweb
     *
     * @param $origins
     * @param $destinations
     *
     * @return string
     */
    function google_distance($origins, $destinations)
    {
        return GoogleDistance::calculate($origins, $destinations);
    }
}
