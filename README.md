# Google Distance in Laravel

## Requirements

- PHP >= 7.1.3
- Laravel >= 5.5.\*

## Installation

Require this package with composer.

```bash
composer require hpsweb/laravel-google-distance-matrix
```

To publishes config `config/google-distance.php`, use command:

```bash
php artisan vendor:publish --tag="laravel-google-distance"
```

You must set your [Google Maps API Key](https://developers.google.com/maps/documentation/distance-matrix/get-api-key) GOOGLE_MAPS_DISTANCE_API_KEY in your .env file like so:

```bash
GOOGLE_MAPS_DISTANCE_API_KEY=ThisIsMyGoogleApiKeyHere
```

## Usage

```php
// Use Facades
use Hpsweb\GoogleDistance\Facades\GoogleDistance;

$distance = GoogleDistance::calculate('FromAddress', 'To Address');

// Use Helper Function
$distance = google_distance('From Address', 'To Address');
```

## Test

```bash
composer test
```

For more info, please visit https://developers.google.com/maps/documentation/distance-matrix/
