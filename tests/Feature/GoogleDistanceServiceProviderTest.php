<?php

namespace Hpsweb\GoogleDistance\Tests\Feature;

use Illuminate\Support\ServiceProvider;
use Hpsweb\GoogleDistance\Providers\GoogleDistanceServiceProvider;
use Hpsweb\GoogleDistance\Tests\TestCase;

class GoogleDistanceServiceProviderTest extends TestCase
{
    /** @test */
    public function it_can_be_constructed()
    {
        $this->assertInstanceOf(ServiceProvider::class, new GoogleDistanceServiceProvider($this->app));
    }
}
