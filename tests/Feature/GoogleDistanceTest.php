<?php

namespace Hpsweb\tests;

use Hpsweb\GoogleDistance\GoogleDistance;
use Hpsweb\GoogleDistance\Tests\TestCase;

use GuzzleHttp\Client;
class GoogleDistanceTest extends TestCase
{
    /** @test */
    public function it_can_created()
    {
        $client = new Client();
        $googleDistanceApi = new GoogleDistance($client);

        $this->assertIsObject($googleDistanceApi);
    }

    /** @test */
    public function google_api_key_was_wrong_or_over_query_limit()
    {
        $client = new Client();
        $distance = (new GoogleDistance($client))
            ->setApiKey('AIzaSyDUBwARxGQIzxglfcl1nm6wQZVrHjiOBuY')
            ->setLanguage('pt-BR')
            ->setRegion('BR')
            ->setUnits('metric')
            ->calculate('Salvador, Bahia', 'Salvador, Bahia');

        var_dump($distance);
        $this->assertIsArray($distance);
    }

    /** @test */
    public function origins_address_is_wrong()
    {
        $client = new Client();
        $distance = (new GoogleDistance($client))
            ->setApiKey('AIzaSyDUBwARxGQIzxglfcl1nm6wQZVrHjiOBuY')
            ->setLanguage('pt-BR')
            ->setRegion('BR')
            ->setUnits('metric')
            ->calculate('', 'Tapiramuta, Bahia');

        $this->assertEquals([], $distance);
    }

    /** @test */
    public function destinations_address_is_wrong()
    {
        $client = new Client();
        $distance = (new GoogleDistance($client))
            ->setApiKey('AIzaSyDUBwARxGQIzxglfcl1nm6wQZVrHjiOBuY')
            ->setLanguage('pt-BR')
            ->setRegion('BR')
            ->setUnits('metric')
            ->calculate('Salvador, Bahia', '');

        $this->assertEquals([], $distance);
    }

    /** @test */
    public function origins_address_and_destinations_address_are_same()
    {
        $client = new Client();
        $distance = (new GoogleDistance($client))
            ->setApiKey('AIzaSyDUBwARxGQIzxglfcl1nm6wQZVrHjiOBuY')
            ->setLanguage('pt-BR')
            ->setRegion('BR')
            ->setUnits('metric')
            ->calculate('Salvador, Bahia', 'Salvador, Bahia');

        $this->assertIsArray($distance);
    }

    /** @test */
    public function origins_address_and_destinations_address_are_an_empty_string()
    {
        $client = new Client();
        $distance = (new GoogleDistance($client))
            ->setApiKey('AIzaSyDUBwARxGQIzxglfcl1nm6wQZVrHjiOBuY')
            ->setLanguage('pt-BR')
            ->setRegion('BR')
            ->setUnits('metric')
            ->calculate('', '');

        $this->assertEquals([], $distance);
    }
}
